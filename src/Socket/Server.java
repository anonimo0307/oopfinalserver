package Socket;

import java.io.*;
import java.net.*;
import java.util.*;
import back.*;

/**
 * <h1>Server</h1>
 * 
 * Server side of the Chatroom
 * <p>
 * 
 * @author Chien-Wei-Chin
 * @version 1.0
 * @since 2017-06-25
 */
public class Server {

	List<PrintStream> output;
	private ServerSocket serverSocket;

	/**
	 * Main method starting server
	 * 
	 * @param args
	 * 			args of main
	 */
	public static void main(String args[]) {
		new Server().start();
	}

	/**
	 * start main function, creating socket for server
	 * and make threads to read client input
	 */
	public void start() {

		output = new ArrayList<>();
		try {

			serverSocket = new ServerSocket(8888);
			while (true) {
				Socket socket = serverSocket.accept();
				PrintStream writer = new PrintStream(socket.getOutputStream());
				output.add(writer);
				Thread t = new Thread(new ServerThread(socket));
				t.start();
				System.out.println(socket.getLocalSocketAddress() + "有" + (Thread.activeCount() - 1) + "個連線");
			}
		} catch (Exception e) {
			System.out.println("connection failed" + e.getMessage());
		}
	}

	/**
	 * <h1>ServerThread</h1>
	 * 
	 * Inner class for thread
	 * <p>
	 * 
	 * @author Chien-Wei-Chin
	 * @version 1.0
	 * @since 2017-06-25
	 */
	public class ServerThread implements Runnable {

		BufferedReader reader;
		Socket sck;

		/**
		 * Constructor for server thread set input reader and output reader
		 * 
		 * @param socket
		 *            the socket of the client
		 */
		public ServerThread(Socket socket) {
			try {
				sck = socket;
				InputStreamReader inReader = new InputStreamReader(sck.getInputStream());

				reader = new BufferedReader(inReader);

			} catch (Exception e) {
				System.out.println("connection failed" + e.getMessage());
			}
		}

		/**
		 * Starting method for thread,read action, user name, password
		 * until he is successfully logined
		 */
		public void run() {
			// two types of ack : login OR register
			String ask;
			String account = null;
			String password;
			PrintStream argotSendtoClient;

			String message;
			try {
				String argot = "--------------";
				while (!argot.equals("access")) {
					ask = reader.readLine();
					account = reader.readLine();
					password = reader.readLine();

					askprocess ask_rs = new askprocess(ask, account, password);
					// System.out.println(argot) ;
					argot = ask_rs.result();
					// System.out.println(argot);
					argotSendtoClient = new PrintStream(sck.getOutputStream(), true);
					argotSendtoClient.println(argot);
					System.out.println(argot);
				}
				while (argot.equals("access") && (message = reader.readLine()) != null) {
					System.out.println("收到" + account + ":" + message);
					broadcast(account + ": " + message);
					if (message.equals("@人數")) {
						broadcast("線上人數：" + Integer.toString(Thread.activeCount() - 1));
					}
				}

			} catch (Exception e) {
				// output.remove(index)
				System.out.println("one connection left " + e.getMessage());
			}
		}

		/**
		 * Broadcast the message to all the online users
		 * 
		 * @param message
		 *            message to be broadcasted
		 */
		public void broadcast(String message) {
			for (PrintStream out : output) {
				try {
					out.println(message);
					out.flush();
				} catch (Exception e) {
					System.out.println("fail to broadcast " + e.getMessage());
				}
			}
		}
	}
}