/**
 * The regprocess program process the register .
 *  
 * @author  b03505051
 * @version 3.0
 * @since   2017-06-11
 */
package back;

import java.sql.SQLException;

public class regprocess {
	/**these are variables*/
	String account ;
	String password ;
	String argot = "repeat" ;
	/**
	 * This is constructor.
	 * */
	public regprocess(String account, String password) {
		this.account = account ;
		this.password = password ;		
	}
	/**
	 * This is a method that tell process result.
	 * @throws ClassNotFoundException, SQLException.
	 * @return String after the process.
	 * */
	public String result(String account ,String password) throws SQLException, ClassNotFoundException {
		if(new CheckDB(account,password).col_acsearch()) return argot ;
		else{
			DBbuild welcome = new DBbuild(account,password) ;
			welcome.establish() ;
			argot = "establish" ;
			return argot ;
		}
	}

}
