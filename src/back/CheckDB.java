/**
 * The CheckDB program implements an application that can check account.
 *  
 * @author  b03505051
 * @version 3.0
 * @since   2017-06-11
 */
package back;

import java.sql.*;

public class CheckDB {
	/**these are variables*/
	String account;
	String password;
	SQLiteConn sql = new SQLiteConn();
	/**
     * This is a constructor connect to database. 
     * @param account is unanimously account.
     * @param password is unanimously password.
     *     
     */
	public CheckDB(String account, String password) throws ClassNotFoundException, SQLException {
		this.account = account;
		this.password = password;
		sql.ConnDB();
	}
	/**
     * This is a method tell the process whether work by T or F 
     * @throws ClassNotFoundException, SQLException
     * @return boolean if password is true.
     */
	public boolean result() throws ClassNotFoundException, SQLException {
		// connect to database
		if(col_pwdsearch()) return true ;
		else return false ;
	}
	/**
     * This is a method search column. 
     * @throws SQLException
     * @return boolean if result is exist.
     */
	public boolean col_acsearch() throws SQLException {
		
		PreparedStatement ac_stt = sql.conn.prepareStatement("SELECT * FROM people WHERE account =?");
		ac_stt.setString(1, account);

		ResultSet ac_rs = ac_stt.executeQuery();
		if(ac_rs.next()) return true ;
		else return false ;
	}
	/**
     * This is a method search column. 
     * @throws SQLException
     * @return boolean if result is exist.
     */
	private boolean col_pwdsearch() throws SQLException {
		PreparedStatement pwd_stt = sql.conn.prepareStatement("SELECT password FROM people WHERE account =?");
		pwd_stt.setString(1, account);
		
		ResultSet pwd_rs = pwd_stt.executeQuery();
		if(pwd_rs.next()==true) {
			String pwd_str = pwd_rs.getString(1) ;
			if(pwd_str.equals(this.password)) {
				return true ;
			}
			else return false ;
		}
		return false ;
	}
}
