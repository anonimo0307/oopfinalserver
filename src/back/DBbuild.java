/**
 * The DBbuild program build database.
 *  
 * @author  b03505051
 * @version 1.0
 * @since   2017-06-01
 */
package back;

import java.sql.*;
public class DBbuild {
	/**these are variables*/
	String account ;
	String password ;
	SQLiteConn sql2 = null;
	
	/**this is default constructor.*/
	public DBbuild(){}
	/**
     * This is constrctor. 
     * @param account is unanimous account.
     * @param password is unanimous account.
     */
	public DBbuild(String account ,String password){
		this.account = account ;
		this.password = password ;
	}
	/**
     * This is a method establishing database. 
     * @throws ClassNotFoundException, SQLException
     */
	public void establish() throws ClassNotFoundException, SQLException{
		sql2 = new SQLiteConn();
		sql2.ConnDB() ;
		//connect to database
		String est_str = "INSERT INTO people"
				+"(account,password)"
				+"VALUES"
				+"(?,?)" ;
		PreparedStatement est_stt = sql2.conn.prepareStatement(est_str) ;
		est_stt.setString(1, this.account) ;
		est_stt.setString(2, this.password) ;
		
		est_stt.executeUpdate() ;
		
	}
	
	
}
