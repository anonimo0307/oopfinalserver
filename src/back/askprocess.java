/**
 * The askprocess program implements ask process.
 *  
 * @author  b03505051
 * @version 2.0
 * @since   2017-06-11
 */
package back;

import java.sql.SQLException;

public class askprocess {
	/**these are variables*/
	String ask ;
	String account ;
	String password ;
	String lp ;
	String rp ;
	String res = "";
	public askprocess (){}
	public askprocess(String ask,String account,String password){
		this.ask = ask ;
		this.account = account ;
		this.password = password ;
		
	}
	/**
     * This is a method handle the different stream. 
     * @throws ClassNotFoundException, SQLException
     * @return String after process.
     */
	public String result() throws ClassNotFoundException, SQLException {
		if(ask.equals("login")){
			loginprocess templog = new loginprocess(this.account ,this.password) ;
			lp = templog.result();
			res = lp ;
			return res ;
			}
		else{
			regprocess tempre = new regprocess(this.account ,this.password) ;
			rp = tempre.result(this.account,this.password) ;
			res = rp ;
			return res ;
			}
	}
}
